/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: BMPImage.h                                   //
// @desc: Some required structures, macros and         //
//        definition of BMPImage class                 //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/

#ifndef UTILS_H
#include "utils.h"
#endif


#ifndef BMPIMAGE_H
#define BMPIMAGE_H

/*** File Load State modes ***/
enum _LoadState
{
    LoadSuccess,
    FileTypeNotValid,
    FileNotExist,
    FileNotReadable
};
/******/

/** Bitmap filetype structures  */
#pragma pack(2)
typedef struct              /**** BMP file header structure ****/
{
    WORD  bfType;           /* Magic number for file */ // 2 bytes
    DWORD bfSize;           /* Size of file */          // 4 bytes
    WORD  bfReserved1;      /* Reserved */              // 2 bytes
    WORD  bfReserved2;      /* Reserved */              // 2 bytes
    DWORD bfOffBits;        /* Offset to bitmap data */ // 4 bytes
} BITMAPFILEHEADER;
#pragma pack()

#define BF_TYPE 0x4D42        /* "BM" */

typedef struct                /**** BMP file info structure ****/
{
    DWORD   bfIHSize;         /* Size of info header  */       // 4 bytes (value = 40 Bytes)
    DWORD   biWidth;          /* Width of image */             // 4 bytes signed int
    DWORD   biHeight;         /* Height of image */            // 4 bytes ...
    WORD    biPlanes;         /* Number of color planes */     // 2 bytes
    WORD    biBitCount;       /* Number of bits per pixel */   // 2 bytes ( color depth )
    DWORD   biCompression;    /* Type of compression to use */ // 4 bytes compression method -> _CompressionMethod
    DWORD   biSizeImage;      /* Size of image data */         // 4 bytes the size of the raw bitmap data (see below), and should not be confused with the file size
    DWORD   biXPelsPerMeter;  /* X pixels per meter */         // 4 bytes the horizontal resolution of the image. (pixel per meter, signed integer)
    DWORD   biYPelsPerMeter;  /* Y pixels per meter */         // 4 bytes the vertical resolution of the image. (pixel per meter, signed integer)
    DWORD   biClrUsed;        /* Number of colors used */      // 4 bytes the number of colors in the color palette, or 0 to default to 2^n.
    DWORD   biClrImportant;   /* Number of important colors */ // 4 bytes the number of important colors used, or 0 when every color is important; generally ignored.
} BITMAPINFOHEADER;

#define HEADER_SIZE sizeof(BITMAPFILEHEADER)
#define INFO_SIZE sizeof(BITMAPINFOHEADER)

class BMPImage
{
public:
    /** Bitmap Header structure **/
    BITMAPFILEHEADER header;
    /** File Header structure **/
    BITMAPINFOHEADER info;
    /** enumrator that contains file load state flags **/
    _LoadState LoadState;
    /** Default destructor */
    virtual ~BMPImage();
    /** Create Picture **/
    bool CreatePicture(const char*, BITMAPFILEHEADER*, BITMAPINFOHEADER*);
    /** Load Picture **/
    bool LoadPicture(char*);
    /** pixels **/
    void LoadPixels();
    /** ofsset and stuff! **/
    unsigned int voidSpaceLength;
    unsigned int lineLength;
    /** Input File Stream **/
    ifstream inputFile;
    /** Output File Pointer **/
    FILE* outputFile;
    /** Draw blue rectangles around faces **/
    void WriteImageData();
    void DetectEdges(BMPImage*);
    void Blur(int);
    /** Picture pixels Matrix **/
    RGBPIXEL** pixelsMatrix;
private:
    /** working file address **/
    char* fileAddress;
};

#endif // BMPIMAGE_H
