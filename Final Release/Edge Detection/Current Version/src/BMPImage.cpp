/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: BMPImage.cpp                                 //
// @desc: class that maniulates bmp images, can read   //
//        from and write to a microsoft bitmap file    //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/

#include "../include/BMPImage.h"

BMPImage::~BMPImage()
{
    inputFile.close();
}

bool BMPImage::LoadPicture(char* address)
{
    fileAddress = strdup(address);

    try
    {
        inputFile.open(fileAddress, ios::in | ios::binary);
    }
    catch (ifstream::failure e)
    {
        LoadState = FileNotExist;
        return false;
    }

    //
    // Read Header
    //
    inputFile.seekg(0, ios::beg);
    inputFile.read(reinterpret_cast<char *>(&header), HEADER_SIZE);
    // Check the file signature
    if (header.bfType != BF_TYPE)
    {
        _L("File type not valid - Bitmap signature (0x%X)", header.bfType)
        LoadState = FileTypeNotValid;
        return false;
    }
    //
    // Read file info
    //
    inputFile.seekg(HEADER_SIZE, ios::beg);
    inputFile.read(reinterpret_cast<char *>(&info), INFO_SIZE);
    // Check compression and color-depth
    if(info.biCompression != 0)
    {
        _L("File type not valid - Compression method not supported")
        LoadState = FileTypeNotValid;
        return false;
    }
    // Color depth
    if(info.biBitCount != 24)
    {
        _L("File type not valid - Bit-Depth not supported")
        LoadState = FileTypeNotValid;
        return false;
    }
    // Color plains
    if(info.biPlanes != 1)
    {
        _L("File type not valid - Number of color planes being used not equal to 1")
        LoadState = FileTypeNotValid;
        return false;
    }


    //
    // Void Space after each row in bitmap binary pixels array
    //
    lineLength = (info.biWidth * sizeof(RGBPIXEL));
    if (lineLength % 4 == 0)
        voidSpaceLength = 0;
    else
        voidSpaceLength = 4 - (lineLength % 4);

    //
    LoadPixels();
    //
    LoadState = LoadSuccess;
    return true;
}


void BMPImage::LoadPixels()
{
    unsigned int w = info.biWidth;
    unsigned int h = info.biHeight;

    // Allocate memory for pixels
    pixelsMatrix = new RGBPIXEL*[h];
    for(unsigned int i = 0 ; i < h ; i++ )
        pixelsMatrix[i] = new RGBPIXEL[w];

    //
    // Get image content
    //
    // Goto where pixel matrix starts
    inputFile.seekg(header.bfOffBits, ios::beg);
    // read pixel by pixel
    for(unsigned int row = h-1 ; row >= 0 ; row--)
    {
        for(unsigned int col = 0 ; col < w ; col++)
            inputFile.read(reinterpret_cast<char *>(&pixelsMatrix[row][col]), sizeof(RGBPIXEL));

        // jump over void space after each row  (happens because of the length = 4*n )
        inputFile.seekg(voidSpaceLength, ios::cur);
    }
}

bool BMPImage::CreatePicture(const char* address, BITMAPFILEHEADER* _header, BITMAPINFOHEADER* _info)
{
    fileAddress = strdup(address);
    //
    if((outputFile = fopen(fileAddress, "wb")) == NULL)
        return false;

    header = *_header;
    info = *_info;
    //
    fwrite(&header, HEADER_SIZE, 1, outputFile);
    fwrite(&info, INFO_SIZE, 1, outputFile);
    //
    unsigned int rows = info.biHeight;
    unsigned int cols = info.biWidth;

    // Allocate memory for pixels
    pixelsMatrix = new RGBPIXEL*[rows];
    for(unsigned int i = 0 ; i < rows ; i++ )
        pixelsMatrix[i] = new RGBPIXEL[cols];
    //
    return true;
}

void BMPImage::WriteImageData()
{
    unsigned int row, rows = info.biHeight;
    unsigned int col, cols = info.biWidth;

    int lineLength = (info.biWidth * sizeof(RGBPIXEL));
    if (lineLength % 4 == 0)
        voidSpaceLength = 0;
    else
        voidSpaceLength = 4 - (lineLength % 4);

    BYTE voidSpace = 0x00;
    fseeko(outputFile, 54, SEEK_SET);
    fseeko(outputFile, 54, SEEK_SET);
    for(row = rows - 1 ; row >= 0 ; row--)
    {
        for(col = 0 ; col < cols ; col++)
            fwrite(&pixelsMatrix[row][col], sizeof(RGBPIXEL), 1, outputFile);

        fwrite(&voidSpace, sizeof(voidSpace), voidSpaceLength / sizeof(voidSpace), outputFile);
    }

    fflush(outputFile);
    fclose(outputFile);
}

inline int gk(RGBPIXEL*a)
{
    return (a->rgbRed+a->rgbGreen+a->rgbBlue)/3;
}


void BMPImage::Blur(int rad)
{

    unsigned int row, rows = info.biHeight;
    unsigned int col, cols = info.biWidth;

    int i,j,k,n;
    for(row = 0 ; row < rows ; row++)
    for(col = 0 ; col < cols ; col++)
    {
        k = n = 0;
        for(i = -rad ; i <= rad ; i++)
        for(j = -rad ; j <= rad ; j++)
        if( row+i >= 0 && row+i < rows && col+j >= 0 && col+j < cols )
        {
            n++;
            k += gk(&pixelsMatrix[row+i][col+j]);
        }
        k /= n;

        pixelsMatrix[row][col].rgbRed = k;
        pixelsMatrix[row][col].rgbGreen = k;
        pixelsMatrix[row][col].rgbBlue = k;
    }
}

void BMPImage::DetectEdges(BMPImage* output)
{
    unsigned int row, rows = info.biHeight;
    unsigned int col, cols = info.biWidth;

    int kx, ky;
    float len;
    int mx[3][3],my[3][3];

    mx[0][0] = -1;mx[0][1] =  0;mx[0][2] = +1;
    mx[1][0] = -2;mx[1][1] =  0;mx[1][2] = +2;
    mx[2][0] = -1;mx[2][1] =  0;mx[2][2] = +1;

    my[0][0] = +1;my[0][1] = +2;my[0][2] = +1;
    my[1][0] =  0;my[1][1] =  0;my[1][2] =  0;
    my[2][0] = -1;my[2][1] = -2;my[2][2] = -1;

    for ( row = 0 ; row < rows ; row++)
    for ( col = 0 ; col < cols ; col++)
    {
        if ( row == 0 || col == 0 || row == rows - 1 || col == cols - 1 )
        {
            output->pixelsMatrix[row][col].rgbRed   = 0x00;
            output->pixelsMatrix[row][col].rgbGreen = 0xff;
            output->pixelsMatrix[row][col].rgbBlue  = 0xff;
        }
        else
        {
            kx = ky = 0;
            for(int i = -1 ; i <= 1 ; i++)
            for(int j = -1 ; j <= 1 ; j++)
            {
                kx += (gk(&pixelsMatrix[row+i][col+j])) * mx[i+1][j+1];
                ky += (gk(&pixelsMatrix[row+i][col+j])) * my[i+1][j+1];
            }

            len = abs(kx) + abs(ky); //~= sqrt( kx*kx + ky*ky );

            //len /= 17.0;
            len = ( len > 255 ? 255 : (len < 0 ? 0 : len) );
            output->pixelsMatrix[row][col].rgbRed = 0;
            output->pixelsMatrix[row][col].rgbGreen = (int)len;
            output->pixelsMatrix[row][col].rgbBlue = 40*len/255;
        }
    }
    return;
}
