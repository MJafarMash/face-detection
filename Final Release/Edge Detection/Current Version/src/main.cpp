/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: main.cpp                                     //
// @desc: contains main function                       //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/

#include "../include/utils.h"
#include "../include/BMPImage.h"

int main(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr<<"wrong arguments"<<endl;
        return 1;
    }
    // Load Picture
    BMPImage image;

    image.LoadPicture(argv[1]);
    //
    string outputFileAddress(argv[1]);
    int lastDot = outputFileAddress.find_last_of(".");
    outputFileAddress = outputFileAddress.substr(0, lastDot);
    outputFileAddress += string(".edge-");
    outputFileAddress += string(argv[2]);
    outputFileAddress += string(".bmp");
    //
    BMPImage outputImage;
    outputImage.CreatePicture(outputFileAddress.c_str(), &image.header, &image.info);
    image.Blur(atoi(argv[2]));
    image.DetectEdges(&outputImage);
    outputImage.WriteImageData();
    //
    return EXIT_SUCCESS;
}
