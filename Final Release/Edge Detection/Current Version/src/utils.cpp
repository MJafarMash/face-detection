/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: utils.cpp                                    //
// @desc: some misc functions                          //
// @author: MohammadJafar MashhadiEbrahim              //
//          MohammadHossein Sharafi                    //
//                                                     //
/*******************************************************/

#include "../include/utils.h"

void GetAddressFromStdin(char* fileAddress)
{
    _P("Enter your bitmap file address: ");
    fileAddress = new char[MAX_ADDRESS_LEN];
    gets(fileAddress);
}

bool ValidAddress(const char* fileAddress)
{
    /** TODO:
        write this code!!
        */
#ifdef LINUX
    return true;
#endif
#ifdef WINDOWS

#endif
    return true;
}

bool FileExists(const char* fileAddress)
{
    return true;
    /** TODO: Fix this, it works incorrectly **/
    //cout << "file exists: " << access( fileAddress , 01 ) << endl;
#ifdef LINUX
    return (access( fileAddress , F_OK ) != -1);
#endif
#ifdef MSWINDOWS
    return (_access( fileAddress , F_OK ) != -1);
#endif
}

bool FileReadable(const char* fileAddress)
{
    return true;
    /** TODO: Fix this, it works incorrectly **/
    //return (access( fileAddress , R_OK ) != -1);
}
