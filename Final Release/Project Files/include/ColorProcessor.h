/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: colorrocessor.h                              //
// @desc: definition of ColorProcessor class           //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/


#ifndef COLORPROCESSOR_H
#define COLORPROCESSOR_H

#include "BMPImage.h"

class ColorProcessor
{
public:
    /** Default constructor **/
    ColorProcessor(BMPImage*);
    /** Default Destructor **/
    ~ColorProcessor();
    /** pixels which are in color of a human skin  **/
    bool** skin;
    /** a pointer to BMPImage class that loaded the image **/
    BMPImage* ptrImage;
    /** Filter Skin color **/
    void FilterSkinColor();
protected:
private:
    /** mathematiccal function that convert colors from RGB color space to rg and HSL colorspaces **/
    void RGBtoHrg(RGBPIXEL*, double&, double&, double&);
};

#endif // COLORPROCESSOR_H
