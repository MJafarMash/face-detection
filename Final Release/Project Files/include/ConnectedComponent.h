/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: ConnectedComponent.h                         //
// @desc: Finds Connected Components                   //
// @author: MohammadHosein Sharafi                     //
//                                                     //
/*******************************************************/

#ifndef UTILS_H
#include "utils.h"
#endif

#ifndef CONNECTEDCOMPONENT_H
#define CONNECTEDCOMPONENT_H

typedef struct
{
    int x;
    int y;
} Point;

struct Component
{
    Point P1;
    // P1 : mokhtassate khoone bala samte rast ast
    Point P2;
    // P2 : mokhtassate khoone payin samte chap ast
};

int getParent(int number, vector<int> parents);             // get parent of number
int checkaround(int around[3][3], vector<int> &parents);      // khoone haye kenar ye khoone ro check mikone ta bebine ghablan meghdar dehi shode ya na khorooji tabe koochak tarin adade khoone haye kenarist
void CountComponents(int **Matrix, int width, int height, int &numberOfComponents,int cmp[]);
// tedade component ha ra mishemarad
Component* GetComponents(int **Matrix, int width, int height, int &numberOfComponents, int counter);
// dar cmp adad hayi ra ke dar Matrix neveshte shode zakhire mishavad
Component* GetConnectedComponents(bool **skinMatrix, int width, int height, int &numberOfComponents );
// araye i az component ha barmigardanad ke be ham mortabet hastand

#endif // CONNECTEDCOMPONENT_H
