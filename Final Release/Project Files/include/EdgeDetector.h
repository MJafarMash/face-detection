/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: EdgeDetector.h                               //
// @desc: Detectes Edges with help of Sobel Algorithm  //
//        (modified)                                   //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/

#include "utils.h"

#ifndef EDGEDETECTOR_H
#define EDGEDETECTOR_H

/** blures picture before edge detection in order to decrease noises **/
int** blur(RGBPIXEL**, unsigned int, unsigned int, int);

/** Detect Edges with `Sobel' algorithm and Store them in a boolean array **/
void DetectEdges(int**, bool**, unsigned int, unsigned int);

#endif // EDGEDETECTOR_H
