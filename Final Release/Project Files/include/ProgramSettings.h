/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: ProgramSettings.h                            //
// @desc: Used for reading from and writing into       //
//        program settings file                        //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/

#include "utils.h"

#ifndef clsSettings_H
#define clsSettings_H

/*** Program settings ***/
typedef struct
{
    bool optDebugMode;

    double optGoldenNumberError;

    unsigned int optComponentAreaMin;
    unsigned int optComponentAreaMax;

    unsigned int optSkinPercentMin;
    unsigned int optSkinPercentMax;

    bool optEdgeDetection;

    bool optMaskPicture;

    unsigned int optBlurRadius;

}_ProgramOptions;
/******/


class clsSettings
{
public:
    /** Default constructor */
    clsSettings(const char*);
    /** Default destructor **/
    ~clsSettings();
    /** Write settings to binary file **/
    void WriteSettings();
    /** reset all options to default values **/
    void ResetToDefault();
    /** Options structure **/
    _ProgramOptions options;
private:
    /** File address **/
    char* SettingsFileName;
    /** create a new settings file and writes default settings values in it **/
    void CreateDefaultSettings(bool write = true);
    /** Load settings from file and save into ``options'' **/
    void LoadSettings();
};

//

extern clsSettings ProgramOptions;

// Functions for options menu :

/** Functions used in [program-executable-name] -o menu **/
void optHandleComponentArea();
void optHandleDebugMode();
void optHandleGoldenNumberError();
void optHandleSkinPercent();
void optHandleEdgeDetecction();
void optHandleBlurRadius();
void optHandleCreateMask();
/** Display all options in stdout **/
void DisplayOptionValues(bool);
/** handle reset, display and change of options **/
void ShowOptionsMenu(bool);

#endif // clsSettings_H
