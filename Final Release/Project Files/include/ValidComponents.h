/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: ValidComponents.h                            //
// @desc: Finds Valid Components                       //
// @author: MohammadHosein Sharafi                     //
//                                                     //
/*******************************************************/


#ifndef VALIDCOMPONENTS_H
#define VALIDCOMPONENTS_H

#include "ConnectedComponent.h"

unsigned int GetSkinPercent(bool **Matrix, Component* thisComponent);
                                                      // tedad khoone haye roshan ra mishemarad
void invert(bool **Matrix, unsigned int width, unsigned int height);
                                                      //khoone haye roshan ra khamoosh va khoone haye khamoosh ra roshan mikonad
bool Checkholes(bool **Matrix,unsigned int width, unsigned int height);
                                                      // soorakh haye tasvir ra barresi mikonad
bool inSpan(double x, double center, double radius);// check kardane vojoode yek adad dar yek baze
bool GoldenRatioTest(unsigned int dx, unsigned int dy);      // test e nesbate talaii
vector<Component> GetValidComponents(bool **skinMatrix ,unsigned int width, unsigned int height,Component* ConnectedComponents, int numberOfComponents, int& numberOfFaces);
                                                      // test haye mokhtalif baraye filter kardane component ha
#endif // VALIDCOMPONENTS_H
