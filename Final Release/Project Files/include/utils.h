/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: utils.h                                      //
// @desc: Almost the most important header file        //
//        all required libraries and type definitions  //
//        and macros and enums and prototype of the    //
//        functions used in utils.cpp file are defined //
//        in this very file!                           //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <functional>
#include <algorithm>
#include <cstring>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>


#ifndef UTILS_H
#define UTILS_H

#include "ProgramSettings.h"

/**** Configure these options before compile ****/

/*** Target operating system ***/
//#define MSWINDOWS
#define LINUX

/**** DO NOT CHANGE ANYTHING HERE! ****/

using namespace std;

/*** File path seperator ***/
#ifdef MSWINDOWS
#define PATH_SEPARATOR '\\'
#endif
#ifdef LINUX
#define PATH_SEPARATOR '/'
#endif

/*** Errors, logging, Promting macros ***/
#define _E(...)   fprintf(stderr, __VA_ARGS__);fprintf(stderr,"\n");
#define _P(...) fprintf(stdout, __VA_ARGS__);
#define _L(...) if (ProgramOptions.options.optDebugMode){\
fprintf(stderr,"> ");\
fprintf(stderr, __VA_ARGS__);\
fprintf(stderr,"\n");\
}

/**** EXIT CODES ****/
#define EXIT_SUCCESS           0

#define EXIT_PARAMETERS_WRONG  64

#define EXIT_FILE_NOT_EXIST    67
#define EXIT_FILE_NOT_READABLE 66

#define EXIT_FILE_TYPE_NOT_VALID 65

#define EXIT_CANNOT_CREATE_OUTPUT 73

/*** Binary type defenitions ***/
#ifdef MSWINDOWS
typedef uint8_t UBYTE;// 1 unsigned byte
typedef __int8  BYTE;  // 1 byte
typedef __int16 WORD;  // 2 bytes
typedef __int32 DWORD; // 4 bytes
typedef __int64 QWORD; // 8 bytes
#endif
#ifdef LINUX
typedef __uint8_t UBYTE;// 1 unsigned byte
typedef int8_t  BYTE;  // 1 byte
typedef int16_t WORD;  // 2 bytes
typedef int32_t DWORD; // 4 bytes
typedef int64_t QWORD; // 8 bytes
#endif
/******/

/*** RGB Structure ***/
typedef struct
{
    UBYTE rgbBlue;     /* Blue value */
    UBYTE rgbGreen;    /* Green value */
    UBYTE rgbRed;      /* Red value */
} RGBPIXEL;
/******/

#ifdef MSWINDOWS
#include <io.h>
#endif

/** Mixes Destination color with source color equally **/
void MixWithColor(RGBPIXEL*, RGBPIXEL*);

/** Checks whether a file exists **/
bool FileExists(const char*);
/** Checks read premission of file **/
bool FileReadable(const char*);

/** Gets program executable name based on argv[0] **/
char* GetExecutableName(char*);
/** Prints command line options help **/
void ShowHelp(const char*);
#endif // UTILS_H
