/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: BMPImage.cpp                                 //
// @desc: class that maniulates bmp images, can read   //
//        from and write to a microsoft bitmap file    //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/

#include "../include/utils.h"
#include "../include/BMPImage.h"

extern clsSettings ProgramOptions;

BMPImage::~BMPImage()
{
    if (inputFile.is_open())
        inputFile.close();

    if (LoadState == LoadSuccess)
    {
        for (int i = 0 ; i < info.biHeight ; i++)
            delete[] pixelsMatrix[i];

        delete[] pixelsMatrix;
    }
    if (fileAddress != NULL)free(fileAddress);
}

bool BMPImage::LoadPicture(char* address)
{
    fileAddress = strdup(address);

    if (!LoadPrechecks())
        return false;
    if (!OpenStream())
        return false;
    if (!LoadHeader())
        return false;
    //
    LoadPixels();
    //
    LoadState = LoadSuccess;
    return true;
}


bool BMPImage::LoadPrechecks()
{
    // existance of file
    if (!FileExists(fileAddress))
    {
        LoadState = FileNotExist;
        return false;
    }

    // readability of file
    if (!FileReadable(fileAddress))
    {
        LoadState = FileNotReadable;
        return false;
    }

    return true;
}

bool BMPImage::OpenStream()
{
    try
    {
        inputFile.open(fileAddress, ios::in | ios::binary);
    }
    catch (ifstream::failure e)
    {
        LoadState = FileNotExist;
        return false;
    }
    return true;
}

bool BMPImage::LoadHeader()
{
    //
    // Read Header
    //
    inputFile.seekg(0, ios::beg);
    inputFile.read(reinterpret_cast<char*>(&header), HEADER_SIZE);
    // Check the file signature
    if (header.bfType != BF_TYPE)
    {
        _L("File type not valid - Bitmap signature (0x%X)", header.bfType)
        LoadState = FileTypeNotValid;
        return false;
    }
    //
    // Read file info
    //
    inputFile.seekg(HEADER_SIZE, ios::beg);
    inputFile.read(reinterpret_cast<char*>(&info), INFO_SIZE);
    // Check compression and color-depth
    if (info.biCompression != 0)
    {
        _L("File type not valid - Compression method is not supported")
        LoadState = FileTypeNotValid;
        return false;
    }
    // Color depth
    if (info.biBitCount != 24)
    {
        _L("File type not valid - Bit-Depth is not supported")
        LoadState = FileTypeNotValid;
        return false;
    }
    // Color plains
    if (info.biPlanes != 1)
    {
        _L("File type not valid - Number of color planes being used is not equal to 1")
        LoadState = FileTypeNotValid;
        return false;
    }

    //
    // Void Space after each row in bitmap binary pixels array
    //
    lineLength = (info.biWidth * sizeof(RGBPIXEL));
    if (lineLength % 4 == 0)
        voidSpaceLength = 0;
    else
        voidSpaceLength = 4 - (lineLength % 4);

    _L("line len: %d\t voidspacelen: %d", lineLength, voidSpaceLength)

    //
    return true;
}

void BMPImage::LoadPixels()
{
    int w = info.biWidth, \
        h = info.biHeight;
    // Allocate memory for pixels
    _L("BMPImage::LoadPixels -> memory allocation")
    pixelsMatrix = new RGBPIXEL*[h];
    for (int i = 0 ; i < h ; i++ )
        pixelsMatrix[i] = new RGBPIXEL[w];

    //
    // Get image content
    //
    // Goto where pixel matrix starts
    _L("BMPImage::LoadPixels -> read from file")
    inputFile.seekg(header.bfOffBits, ios::beg);
    // read pixel by pixel
    for (int row = h-1 ; row >= 0 ; row--)
    {
        for (int col = 0 ; col < w ; col++)
            inputFile.read(reinterpret_cast<char*>(&pixelsMatrix[row][col]), sizeof(RGBPIXEL));

        // jump over void space after each row  (happens because of the length = 4*n )
        inputFile.seekg(voidSpaceLength, ios::cur);
    }

}

bool BMPImage::CreatePicture(const char* address, BITMAPFILEHEADER* _header, BITMAPINFOHEADER* _info)
{
    fileAddress = strdup(address);
    //
    outputFile = fopen(fileAddress, "wb");
    if (outputFile == NULL)
        return false;
    //
    LoadState = LoadSuccess;
    //
    header = *_header;
    info = *_info;
    //
    fwrite(&header, HEADER_SIZE, 1, outputFile);
    fwrite(&info, INFO_SIZE, 1, outputFile);
    // Allocate memory for pixels
    _L("BMPImage::CreatePicture -> memory allocation")
    pixelsMatrix = new RGBPIXEL*[info.biHeight];
    for (int i = 0 ; i < info.biHeight ; i++ )
        pixelsMatrix[i] = new RGBPIXEL[info.biWidth];
    //
    lineLength = (info.biWidth * sizeof(RGBPIXEL));
    if (lineLength % 4 == 0)
        voidSpaceLength = 0;
    else
        voidSpaceLength = 4 - (lineLength % 4);
    //
    return true;
}

void BMPImage::CopyPicture(BMPImage* mainImage)
{
    unsigned int row, rows = mainImage->info.biHeight;
    unsigned int col, cols = mainImage->info.biWidth;

    // copy pixels from source image
    for (row = 0 ; row < rows ; row++)
    for (col = 0 ; col < cols ; col++)
        pixelsMatrix[row][col] = mainImage->pixelsMatrix[row][col];
}

void BMPImage::ShowComponents(vector<Component>* faces,  RGBPIXEL* colorToMix)
{
    if (ProgramOptions.options.optDebugMode)
    {
        for (unsigned int i = 0 ; i < faces->size() ; i++)
            _P("face#%03d : (%3d,%3d) -> (%3d,%3d)\n", i+1 , faces->at(i).P1.x, faces->at(i).P1.y, faces->at(i).P2.x, faces->at(i).P2.y )
    }

    int currentX, currentY;
    for (unsigned int fNumber = 0 ; fNumber < faces->size() ; fNumber++)
    {
        for (currentX = faces->at(fNumber).P1.x ; currentX < faces->at(fNumber).P2.x ; currentX++)
        {
            if (faces->at(fNumber).P1.y+1 < info.biHeight - 1)
                MixWithColor(&pixelsMatrix[faces->at(fNumber).P1.y+1][currentX], colorToMix);
            if (faces->at(fNumber).P2.y+1 < info.biHeight - 1)
                MixWithColor(&pixelsMatrix[faces->at(fNumber).P2.y+1][currentX], colorToMix);

            MixWithColor(&pixelsMatrix[faces->at(fNumber).P1.y  ][currentX], colorToMix);
            MixWithColor(&pixelsMatrix[faces->at(fNumber).P2.y  ][currentX], colorToMix);

            if (faces->at(fNumber).P1.y-1 >= 0)
                MixWithColor(&pixelsMatrix[faces->at(fNumber).P1.y-1][currentX], colorToMix);
            if (faces->at(fNumber).P2.y-1 >= 0)
                MixWithColor(&pixelsMatrix[faces->at(fNumber).P2.y-1][currentX], colorToMix);
        }

        for (currentY = faces->at(fNumber).P1.y ; currentY < faces->at(fNumber).P2.y ; currentY++)
        {
            if (faces->at(fNumber).P1.x+1 < info.biWidth - 1)
                MixWithColor(&pixelsMatrix[currentY][faces->at(fNumber).P1.x+1], colorToMix);
            if (faces->at(fNumber).P2.x+1 < info.biWidth - 1)
                MixWithColor(&pixelsMatrix[currentY][faces->at(fNumber).P2.x+1], colorToMix);

            MixWithColor(&pixelsMatrix[currentY][faces->at(fNumber).P1.x], colorToMix);
            MixWithColor(&pixelsMatrix[currentY][faces->at(fNumber).P2.x], colorToMix);

            if (faces->at(fNumber).P1.x-1 >= 0)
                MixWithColor(&pixelsMatrix[currentY][faces->at(fNumber).P1.x-1], colorToMix);
            if (faces->at(fNumber).P2.x-1 >= 0)
                MixWithColor(&pixelsMatrix[currentY][faces->at(fNumber).P2.x-1], colorToMix);
        }
    }
}

void BMPImage::WriteImageData()
{
    int row, rows = info.biHeight;
    int col, cols = info.biWidth;

    BYTE voidSpace = 0x00;
    for (row = rows-1 ; row >= 0 ; row--)
    {
        for (col = 0 ; col < cols ; col++)
            fwrite(&pixelsMatrix[row][col], sizeof(RGBPIXEL), 1, outputFile);

        fwrite(&voidSpace, sizeof(voidSpace), voidSpaceLength / sizeof(voidSpace), outputFile);
    }

    fflush(outputFile);
    fclose(outputFile);
}

template <typename T>void inline prf(const char s[], T x)
{fprintf(stdout,"# \t%s: 0x%X (=%d)\n", s, x, x);}
void BMPImage::PrintFileInfo()
{
    _P("## HEADER_SIZE: %luB -- INFO_SIZE: %luB\n", HEADER_SIZE, INFO_SIZE);
    fprintf(stdout, "##Header 1\n");
    prf("Signature           ", header.bfType);
    prf("Size of file        ", header.bfSize);
    prf("Reserved            ", header.bfReserved1);
    prf("Reserved            ", header.bfReserved2);
    prf("Bitmap data offset  ", header.bfOffBits);
    fprintf(stdout, "##Header 2\n");
    prf("Size of info header ", info.bfIHSize);
    prf("Width               ", info.biWidth);
    prf("Height              ", info.biHeight);
    prf("Color planes        ", info.biPlanes);
    prf("Color Depth         ", info.biBitCount);
    prf("Compression method  ", info.biCompression);
    prf("Size of image data  ", info.biSizeImage);
    prf("X pixels per meter  ", info.biXPelsPerMeter);
    prf("Y pixels per meter  ", info.biYPelsPerMeter);
    prf("Colors used         ", info.biClrUsed);
    prf("Important colors    ", info.biClrImportant);

    fflush(stdout);
}
