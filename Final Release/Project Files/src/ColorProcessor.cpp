/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: colorprocessor.cpp                           //
// @desc: class that converts color systems and process//
//        colors to mach skin color                    //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/

#include "../include/ColorProcessor.h"

ColorProcessor::ColorProcessor(BMPImage* _ptrImage)
{
    ptrImage = _ptrImage;
    int w = ptrImage->info.biWidth, \
        h = ptrImage->info.biHeight;

    skin = new bool*[h];
    for (int i = 0 ; i < h ; i++ )
        skin[i] = new bool[w];
}

ColorProcessor::~ColorProcessor()
{
    for (int i = 0 ; i < ptrImage->info.biHeight ; i++ )
        delete[] skin[i];

    delete[] skin;
}

void ColorProcessor::RGBtoHrg(RGBPIXEL* rgbPix, double& hslHue, double& rgRed, double& rgGreen)
{
    //rg
    rgRed   = 1.f*rgbPix->rgbRed   / (rgbPix->rgbRed + rgbPix->rgbGreen + rgbPix->rgbBlue);
    rgGreen = 1.f*rgbPix->rgbGreen / (rgbPix->rgbRed + rgbPix->rgbGreen + rgbPix->rgbBlue);

    //HSL
    double rDensity = (rgbPix->rgbRed)   / 255.f;
    double gDensity = (rgbPix->rgbGreen) / 255.f;
    double bDensity = (rgbPix->rgbBlue)  / 255.f;
    //
    double maxDensity = max(rDensity, max(gDensity, bDensity));
    double minDensity = min(rDensity, min(gDensity, bDensity));
    double delta = maxDensity - minDensity;
    //

    if (delta == 0) // maxDensity == minDensity
    {
        hslHue = 0;
        return;
    }

    if ( rDensity == maxDensity )       // pure red
        hslHue = (gDensity - bDensity) / delta;

    else if ( gDensity == maxDensity ) // pure green
        hslHue = 2 + ((bDensity - rDensity) / delta);

    else if ( bDensity == maxDensity ) // pure blue
        hslHue = 4 + ((rDensity - gDensity) / delta);

    //
    hslHue += (hslHue < 0) ? 6 : 0;
    hslHue *= 40;
    //
    return;
}

void ColorProcessor::FilterSkinColor()
{
    int row=0, rows = ptrImage->info.biHeight;
    int col=0, cols = ptrImage->info.biWidth;

    RGBPIXEL thisPixel;

    double h = 0, r = 0, g = 0;

    for (row = 0 ; row < rows ; row++)
    for (col = 0 ; col < cols ; col++)
    {
        thisPixel = ptrImage->pixelsMatrix[row][col];
        RGBtoHrg(&thisPixel, h, r, g);
        // this condition is the least calculation required condition, so it's better to test
        // it before start calculations on r and g. in the previous vesion there was only one
        // conditional-statement.
        skin[row][col] = false;
        if (h<20 || h>=239)
        {
            if ( -0.776*r*r + 0.5601*r + 0.18 < g                        /* F2 < g       */ \
              && g < -1.376*r*r + 1.0743*r + 0.2                         /*      g < F1  */ \
              && (r - 0.33)*(r - 0.32) + (g - 0.33)*(g - 0.33) > 0.005 ) /* w > 0.005    */
                skin[row][col] = true;
        }
    }
}
