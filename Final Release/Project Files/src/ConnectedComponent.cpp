/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: ConnectedComponent.h                         //
// @desc: Finds Connected Components                   //
// @author: MohammadHosein Sharafi                     //
//                                                     //
/*******************************************************/

#include "../include/ConnectedComponent.h"

int getParent(int number, vector<int> parents)
{
    //get parent of number
    if (parents.at(number) == number) return number;

    return getParent(parents.at(number), parents);
}

int checkaround(int around[3][3], vector<int> &parents)
{
    // khoone haye kenar ye khoone ro check mikone ta bebine ghablan meghdar dehi shode ya na
    // khorooji tabe koochak tarin adade khoone haye kenarist
    int i,j;
    int min = 0;

    //koochek tarin khone mojaver ra miyabad
    for (i = 0 ; i < 3 ; i++)
    for (j = 0 ; j < 3 ; j++)
        if (around[i][j] != 0 && ( (min == 0) || ( min != 0  && around[i][j] < min) ))
            min = around[i][j];

    if (min == 0) return 0;

    //parent khoone haye kenari ra avaz mikonad
    for (i = 0 ; i < 3 ; i++)
    for (j = 0 ; j < 3 ; j++)
        if (around[i][j] != 0)
            parents[around[i][j]] = min;

    return min;
}

void CountComponents(int **Matrix, int width, int height, int &numberOfComponents, int cmp[])
{
    // moteghayer haye halghe
    int i,j;
    //tedade component ha ra mishemarad
    int k;
    for (i = 0 ; i < height ; i++)
    for (j = 0 ; j < width  ; j++)
        if (Matrix[i][j] != 0)
        {
            k = 1;
            // check mikonad in adad ghablan shemorde shode ya na.
            while (cmp[k] != Matrix[i][j] && k <= numberOfComponents)
                k++;

            // aghar shemorde nashode bood be numberOfComponents miafzayad va an ra dar cmp zakhire mikonad
            if (k > numberOfComponents)
                cmp[++numberOfComponents] = Matrix[i][j];
        }

    return;
}

Component* GetComponents(int **Matrix, int width, int height, int &numberOfComponents, int counter)
{
    // dar cmp adad hayi ra ke dar Matrix neveshte shode zakhire mishavad
    int *cmp = new int[counter];
    numberOfComponents = 0;
    // tedad component ha mohasebe mishavad
    CountComponents(Matrix,width,height,numberOfComponents,cmp);
    // jaye component ha dar ConnectedComponent zakhire mishavad
    Component* ConnectedComponent = new Component[numberOfComponents];
    int i, j, k;
    for (k = 0 ; k < numberOfComponents ; k++)
    {
        // dar ebteda halat hadi baraye ConnectedComponent dar nazar gerefte mishavad
/*
+-------------------------------------------------------+
|                                                       |
|     _/----\                                           |
|    /       |                     Bounding Box         |
|    | comp   \                                         |
|    | onent  |                                         |
|     \______/                                          |
|                                                       |
|                                                       |
|                                                       |
+-------------------------------------------------------+
*/
        ConnectedComponent[k].P2.x = 0;
        ConnectedComponent[k].P2.y = 0;
        ConnectedComponent[k].P1.x = width;
        ConnectedComponent[k].P1.y = height;

        // tamame khoone ha peymayesh mishavad
        for (i = 0 ; i < height ; i++)
        for (j = 0 ; j < width  ; j++)
            //khoone haye marboot be component mored nazar barresi mishavad
            if (Matrix[i][j] == cmp[k])
            {
                //agar khoone dar component feli gharar nadasht mokhtassat ra eslah mikonad
                if (j < ConnectedComponent[k].P1.x)    ConnectedComponent[k].P1.x = j;
/*
    +---------------------------------------------------+
    |                                                   |
    | _/----\                                           |
    |/       |                     Bounding Box         |
    || comp   \                                         |
    || onent  |                                         |
    | \______/                                          |
    |                                                   |
    |                                                   |
    |                                                   |
    +---------------------------------------------------+
*/

                if (i < ConnectedComponent[k].P1.y)    ConnectedComponent[k].P1.y = i;
/*

    +---------------------------------------------------+
    | _/----\                                           |
    |/       |                     Bounding Box         |
    || comp   \                                         |
    || onent  |                                         |
    | \______/                                          |
    |                                                   |
    |                                                   |
    |                                                   |
    +---------------------------------------------------+
*/
                if (j > ConnectedComponent[k].P2.x)    ConnectedComponent[k].P2.x = j;
/*

    +----------+
    | _/----\  |
    |/       | |
    || comp   \|
    || onent  ||
    | \______/ |
    |Bounding  |
    |  Box     |
    |          |
    +----------+
*/
                if (i > ConnectedComponent[k].P2.y)    ConnectedComponent[k].P2.y = i;
/*

    +----------+
    | _/----\  |
    |/       | |
    || comp   \|
    || onent  ||
    | \______/ |
    +----------+
     Correct
      Bounding Box

*/
            }
    }

    //mokhtassat Matrix ra be mokhtassat skinMatrix tabdil mikonad
    for (j = 0; j < numberOfComponents ; j++)
    {
        ConnectedComponent[j].P1.x--;
        ConnectedComponent[j].P1.y--;
        ConnectedComponent[j].P2.x--;
        ConnectedComponent[j].P2.y--;
    }

    delete[] cmp;

    for (i = 0 ; i < height ; i++)
        delete[] Matrix[i];
    delete[] Matrix;
    //araye ei az component haye tayid shode barmigardunad
    return ConnectedComponent;

}

Component* GetConnectedComponents(bool **skinMatrix, int width, int height, int &numberOfComponents )
{
    // moteghayer haye halghe
    int i,j,k,l;
    //parent har adad ra zakhire mikonad
    vector<int> parents;
    parents.push_back(0);
    // araye E az component ha barmigardanad ke be ham mortabet hastand

    //araye E Ejad mikonad ba 2 radif va sotoon bishtar az skinMatrix
    int **Matrix = new int*[height + 2];
    for (i = 0 ; i < height + 2 ; i++)
        Matrix[i]= new int[width + 2];

    //tamam khoone haye araye ra 0 mikonad
    for (i = 0 ; i < height + 2 ; i++)
    for (j = 0 ; j < width  + 2 ; j++)
        Matrix[i][j] = 0;

    //khoone haye kenar khoone moored barresi ra dar khod zakhire mikonad
    int around[3][3];
    //Akharin adadi ke dar Matrix gharar gerefte
    int counter = 0;

    //roshan boodane khoone ra barresi mikonad
    for (i = 0 ; i < height ; i++)
    for (j = 0 ; j < width  ; j++)
        if (skinMatrix[i][j])
        {
            // around khoone ra misazad
            // around:
            //:::: : ::::
            //...O O O...
            //...O X O...
            //...O O O...
            //:::: : ::::
            //  X ~ a[i][j]
            for (k = 0 ; k < 3 ; k++)
            for (l = 0 ; l < 3 ; l++)
                around[k][l] = Matrix[i+k][j+l];

            //agar hamsaye haye khoone ghablan meghdar dehi shode bood , meghdarash ra kamtarin Anha migozarad
            if (checkaround(around, parents) != 0)
                Matrix[i+1][j+1] = checkaround(around, parents);
            else
            {
                //agar hamsaye haye khoone ghablan meghdar dehi shode bood ,baraye An meghdar jadid gharar midahad
                Matrix[i+1][j+1] = ++counter;
                parents.resize(parents.size());
                parents.push_back(counter);
            }

        }
    // meghdar har khoone ra barabar Akharin parentash migozarad
    for (i = 1 ; i < height + 1 ; i++)
    for (j = 1 ; j < width  + 1 ; j++)
        if (Matrix[i][j] != 0)
            Matrix[i][j] = getParent(Matrix[i][j], parents);

    //component hara tashkil midahad
    return GetComponents (Matrix, width+2, height+2, numberOfComponents, counter) ;
}
