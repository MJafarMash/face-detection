/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: EdgeDetector.cpp                             //
// @desc: Detectes Edges with help of Sobel Algorithm  //
//        (modified)                                   //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/

#include "../include/EdgeDetector.h"


// detect and merge
void DetectEdges(int** pixelsMatrix, bool** outputMatrix, unsigned int cols, unsigned int rows)
{
    int GX[3][3],GY[3][3];

    GX[0][0] = -1;GX[0][1] =  0;GX[0][2] = +1;
    GX[1][0] = -2;GX[1][1] =  0;GX[1][2] = +2;
    GX[2][0] = -1;GX[2][1] =  0;GX[2][2] = +1;

    GY[0][0] = +1;GY[0][1] = +2;GY[0][2] = +1;
    GY[1][0] =  0;GY[1][1] =  0;GY[1][2] =  0;
    GY[2][0] = -1;GY[2][1] = -2;GY[2][2] = -1;


    unsigned int row, col;
    int  i, j;
    int kx, ky;
    for ( row = 0 ; row < rows ; row++ )
    for ( col = 0 ; col < cols ; col++ )
    {
        if ( row == 0 || col == 0 || row == rows - 1 || col == cols - 1 )
            outputMatrix[row][col] = false;
        else
        {
            kx = ky = 0;
            for ( i = -1 ; i <= 1 ; i++ )
            for ( j = -1 ; j <= 1 ; j++ )
            {
                kx += pixelsMatrix[row+i][col+j] * GX[i+1][j+1];
                ky += pixelsMatrix[row+i][col+j] * GY[i+1][j+1];
            }
            outputMatrix[row][col] = outputMatrix[row][col] && (abs(kx) + abs(ky) > 120 ? false : true);
        }
    }

    for (i = 0 ; i < (signed)rows ; i++)
        delete[] pixelsMatrix[i];
    delete[] pixelsMatrix;

    return;
}

// blures picture
int** blur(RGBPIXEL** pixelsMatrix, unsigned int cols, unsigned int rows, int rad)
{
    unsigned int row, col;
    int i,j,k,n;

    int** outputPixelsMatrix = new int*[rows];
    for ( i=0 ; i<(signed)rows ; i++)
        outputPixelsMatrix[i] = new int[cols];

    for (row = 0 ; row < rows ; row++)
    for (col = 0 ; col < cols ; col++)
    {
        k = n = 0;
        for (i = -rad ; i <= rad ; i++)
        for (j = -rad ; j <= rad ; j++)
        if ( row+i >= 0 && row+i < rows && col+j >= 0 && col+j < cols )
        {
            n++;
            k += (pixelsMatrix[row+i][col+j].rgbRed + pixelsMatrix[row+i][col+j].rgbGreen + pixelsMatrix[row+i][col+j].rgbBlue ) / 3;
        }
        k /= n;

        outputPixelsMatrix[row][col] = k;
    }

    return outputPixelsMatrix;
}
