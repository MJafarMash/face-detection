/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: ProgramSettings.cpp                          //
// @desc: Used for reading from and writing into       //
//        program settings file                        //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/

#include "../include/ProgramSettings.h"

clsSettings::clsSettings(const char* s)
{
    SettingsFileName = strdup(s);
    LoadSettings();
}

clsSettings::~clsSettings()
{
    free(SettingsFileName);
}

void clsSettings::LoadSettings()
{
    FILE* settingsFileHandler;
    if (!FileExists(SettingsFileName))
    {
        fprintf(stderr, "> Settings File Missing\n");
        CreateDefaultSettings();
    }
    if (!FileReadable(SettingsFileName))
    {
        fprintf(stderr, "> Settings File is not readable by this program\n");
        CreateDefaultSettings(false);
    }
    settingsFileHandler = fopen(SettingsFileName, "rb");
    fseek(settingsFileHandler, 0, SEEK_SET);
    if (fread(&options, sizeof(_ProgramOptions), 1, settingsFileHandler) != 1)
    {
        _E("Fatal error, cannot read from settings file(%s).", SettingsFileName)
        return;
    }
    fclose(settingsFileHandler);
}

void clsSettings::CreateDefaultSettings(bool write)
{
    options.optDebugMode = false;
    options.optGoldenNumberError = 0.65;

    options.optComponentAreaMin = 1000;
    options.optComponentAreaMax = 8000;

    options.optSkinPercentMin = 35;
    options.optSkinPercentMax = 75;

    options.optEdgeDetection = false;

    options.optMaskPicture = false;

    options.optBlurRadius = 0;
    //
    if (write)
        WriteSettings();
}

void clsSettings::WriteSettings()
{
    FILE* settingsFileHandler;
    if (!(settingsFileHandler = fopen(SettingsFileName, "wb+")))
    {
        _E("Fatal error, Cannot create settings file")
        return;
    }

    fseek(settingsFileHandler, 0, SEEK_CUR);
    fwrite(&options, sizeof(_ProgramOptions), 1, settingsFileHandler);
    fflush(settingsFileHandler);
    fclose(settingsFileHandler);

    LoadSettings();
}

void clsSettings::ResetToDefault()
{
    this->CreateDefaultSettings();
}


////////////////////////////////////////////////


void ShowOptionsMenu(bool backOption)
{
    DisplayOptionValues(backOption);
    //
    // now  we make edits
    //
    string stropt;
    while (true)
    {
#ifdef LINUX
        _P("\033[1m");
#endif
        _P("Enter option number you want to change: ");
#ifdef LINUX
        _P("\033[0m");
#endif
        // Get option ID
        cin >> stropt;
        // Calls right function to handle option
             if (stropt == "1")
            optHandleDebugMode();
        else if (stropt == "2")
            optHandleEdgeDetecction();
        else if (stropt == "3")
            optHandleGoldenNumberError();
        else if (stropt == "4")
            optHandleComponentArea();
        else if (stropt == "5")
            optHandleSkinPercent();
        else if (stropt == "6")
        {
            if (ProgramOptions.options.optEdgeDetection)
                optHandleBlurRadius();
            else
                _E("Since Edge Detection is disabled, blur radius cannot be changed");
        }
        else if (stropt == "7")
            optHandleCreateMask();
        else if (stropt == "r" ||
                 stropt == "R" ||
                 stropt == "reset")
                ProgramOptions.ResetToDefault();
        else if (stropt == "e" ||
                 stropt == "E" ||
                 stropt == "exit" ||
                 stropt == "0" ||
                 stropt == "back" ||
                 stropt == "b" ||
                 stropt == "B")
                 break;
        else if (stropt == "display" ||
                 stropt == "show" ||
                 stropt == "d" || stropt == "D" ||
                 stropt == "s" || stropt == "S")
                 DisplayOptionValues(backOption);
        else
            _E("%s is not a valid option", stropt.c_str());
    }

    ProgramOptions.WriteSettings();
}

void DisplayOptionValues(bool backOption)
{
    /// writing field width
    const unsigned short int w = 35;
    /// Actions
    if (backOption)
        cout << "[B]ack" << endl;
    else
        cout << "[E]xit" << endl;

    cout << "[R]eset to default" << endl << \
            "[D]isplay new options values" << endl;


    /// Debug mode
    cout << left << setw(w) << "[1] Debug Mode:";
    if (ProgramOptions.options.optDebugMode)
        cout << "Enabled";
    else
        cout << "Disabled";
    cout << endl;
    /// Edge detection
    cout << left << setw(w) << "[2] Edge Detection:";
    if (ProgramOptions.options.optEdgeDetection)
        cout << "Enabled";
    else
        cout << "Disabled";
    cout << endl;
    /// GoldenNumber Error
    cout << left << setw(w) << "[3] Golden number error:";
    cout << ProgramOptions.options.optGoldenNumberError;
    cout << endl;
    /// Acceptable component area
    cout << left << setw(w) << "[4] Acceptable Component Area:";
    #ifdef MSWINDOWS
    cout << right << setw(5) << ProgramOptions.options.optComponentAreaMin << " to " << right << setw(5) << ProgramOptions.options.optComponentAreaMax;
    #endif
    #ifdef LINUX
    cout << right << "\033[4m" << setfill(' ') << setw(5) << ProgramOptions.options.optComponentAreaMin << "\033[0m" << " to " << right << "\033[4m" << setfill(' ') << setw(5) << ProgramOptions.options.optComponentAreaMax<< "\033[0m" ;
    #endif
    cout << endl;
    /// Acceptable skin percent
    cout << left << setw(w) << "[5] Acceptable Skin percent:";
    #ifdef MSWINDOWS
    cout << right << setw(2) << ProgramOptions.options.optSkinPercentMin << " to " << right << setw(3) << ProgramOptions.options.optSkinPercentMax;
    #endif
    #ifdef LINUX
    cout << right << "\033[4m" << setfill(' ') << setw(3) << ProgramOptions.options.optSkinPercentMin << "\033[0m" << " to " << right << "\033[4m" << setfill(' ') << setw(3) << ProgramOptions.options.optSkinPercentMax<< "\033[0m" ;
    #endif
    cout << endl;
    /// Blur radius (blur will apply before edge detection)
    if (ProgramOptions.options.optEdgeDetection)
        cout << left << setw(w) << "[6] Blur Radius:";
    else
        cout << left << setw(w) << "[ ] Blur Radius:";
    cout << ProgramOptions.options.optBlurRadius;
    cout << endl;
    /// Create mask picture
    cout << left << setw(w) << "[7] Create Mask Picture:";
    if (ProgramOptions.options.optMaskPicture)
        cout << "Enabled";
    else
        cout << "Disabled";
    cout << endl;
}

void optHandleCreateMask()
{
    bool retry = false;

    string input;
    do
    {
        retry = false;
        _P("New value> ")
        cin >> input;
        transform(input.begin(), input.end(), input.begin(), ::tolower);

        if (input == "true" || input == "on" || input == "1" || input == "enable")
            ProgramOptions.options.optMaskPicture = true;
        else if (input == "false" || input == "off" || input == "0" || input == "disable")
            ProgramOptions.options.optMaskPicture = false;
        else
        {
            _E("Wrong parameter, enter either \"on, enable, true, 1\" or \"off, disable, false, 0\"")
            retry = true;
        }
    }
    while(retry);
}

void optHandleDebugMode()
{
    bool retry = false;

    string input;

    do
    {
        retry = false;
        _P("New value> ")
        cin >> input;
        transform(input.begin(), input.end(), input.begin(), ::tolower);

        if (input == "true" || input == "on" || input == "1" || input == "enable")
            ProgramOptions.options.optDebugMode = true;
        else if (input == "false" || input == "off" || input == "0" || input == "disable")
            ProgramOptions.options.optDebugMode = false;
        else
        {
            _E("Wrong parameter, enter either \"on, enable, true, 1\" or \"off, disable, false, 0\"")
            retry = true;
        }
    }
    while(retry);
}

void optHandleGoldenNumberError()
{
    bool retry = false;

    string input;
    do
    {
        retry = false;
        _P("New value> ")
        cin >> input;

        double dblInput = fabs(atof(input.c_str()));
        if (dblInput > 1)
        {
            _E("Wrong value(too high). Error must be less than 1.")
            retry = true;
        }
        else
            ProgramOptions.options.optGoldenNumberError = dblInput;
    }
    while(retry);
}

void optHandleComponentArea()
{
    bool retry = false;

    string input;
    int minAA, maxAA;
    do
    {
        retry = false;
        _P("New values. Separated with space>");

        cin >> input;
        minAA = abs(atoi(input.c_str()));

        cin >> input;
        maxAA = abs(atoi(input.c_str()));

        if (minAA > maxAA)
        {
            _E("Minimum is greater than maximum, swaping values.")
            swap(minAA, maxAA);
        }

        if (minAA < 50)
            _E("Wrong value(minimum too low). Enter a number greater than 50.")

        if (maxAA > 10000)
            _E("Wrong value(maximum too high). Enter a number less than 10000.")

        if (maxAA > 10000 || minAA < 50)
            retry = true;
    }
    while (retry);

    ProgramOptions.options.optComponentAreaMin = minAA;
    ProgramOptions.options.optComponentAreaMax = maxAA;
}

void optHandleSkinPercent()
{
    bool retry = false;

    string input;
    int minSP, maxSP;
    do
    {
        retry = false;
        _P("New values. Separated with space>");

        cin >> input;
        minSP = abs(atoi(input.c_str()));

        cin >> input;
        maxSP = abs(atoi(input.c_str()));

        if (minSP > maxSP)
        {
            _E("Minimum is greater than maximum, swaping values.")
            swap(minSP, maxSP);
        }

        if (minSP < 20)
            _E("Wrong value(minimum too low). Enter a number greater than 20.")

        if (maxSP > 95)
            _E("Wrong value(maximum too high). Enter a number less than 95.")

        if (maxSP > 95 || minSP < 20)
            retry = true;
    }
    while(retry);

    ProgramOptions.options.optSkinPercentMin = minSP;
    ProgramOptions.options.optSkinPercentMax = maxSP;
}

void optHandleEdgeDetecction()
{
    bool retry = false;

    string input;
    do
    {
        retry = false;
        _P("New value> ")
        cin >> input;
        transform(input.begin(), input.end(), input.begin(), ::tolower);

        if (input == "true" || input == "on" || input == "1" || input == "enable")
            ProgramOptions.options.optEdgeDetection = true;
        else if (input == "false" || input == "off" || input == "0" || input == "disable")
            ProgramOptions.options.optEdgeDetection = false;
        else
        {
            _E("Wrong value, enter either \"on, enable, true, 1\" or \"off, disable, false, 0\"")
            retry = true;
        }
    }
    while(retry);
}

void optHandleBlurRadius()
{
    bool retry = false;
    string input;
    do
    {
        retry = false;
        _P("New value> ")
        cin >> input;

        unsigned int intInput = abs(atoi(input.c_str()));
        if (intInput > 5)
            _P("Wrong value(too high). cannot be changed")
        else
            ProgramOptions.options.optBlurRadius = intInput;

    }
    while (retry);

}
