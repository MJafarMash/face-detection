/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: ValidComponents.cpp                          //
// @desc: Finds Valid Components                       //
// @author: MohammadHosein Sharafi                     //
//                                                     //
/*******************************************************/

#include "../include/ValidComponents.h"

extern clsSettings ProgramOptions;

unsigned int GetSkinPercent(bool **Matrix, Component* thisComponent)
{
    // tedad khoone haye roshan ra mishemarad
    unsigned int numberOfWhites = 0;
    unsigned int dy = thisComponent->P2.y - thisComponent->P1.y;
    unsigned int dx = thisComponent->P2.x - thisComponent->P1.x;

    for (unsigned int i=0 ; i<dx ; i++)
    for (unsigned int j=0 ; j<dy ; j++)
        if (Matrix[j][i])
            numberOfWhites++;

    return 100*numberOfWhites/(dy * dx);
}

void invert(bool **Matrix, unsigned int width, unsigned int height)
{
    //khoone haye roshan ra khamoosh va khoone haye khamoosh ra roshan mikonad
    for (unsigned int i=0; i<height; i++)
    for (unsigned int j=0; j<width; j++)
        Matrix[i][j]=!Matrix[i][j];

    return;
}


bool Checkholes(bool **Matrix, unsigned int width, unsigned int height)
{
    // soorakh haye tasvir ra barresi mikonad
    int numberOfHoles;
    //khoone haye roshan ra khamoosh va khoone haye khamoosh ra roshan mikonad
    invert(Matrix, width, height);
    //jahe soorakh ha ra moshakhas mikonad
    Component* holes = GetConnectedComponents(Matrix, width, height, numberOfHoles);
    //andaze soorakh ra barresi mikonad

    float Min = 0.001*width*height;
    float Max = 0.1    *width*height;
    float Area;

    for (int i = numberOfHoles - 1 ; i >= 0 ; i--)
    {
        //agar hole kenar component bood hazfash mikonad
        if(holes[i].P1.x == 0 ||holes[i].P1.y == 0 || holes[i].P2.x == width - 1 || holes[i].P2.y == height - 1)
        {
            numberOfHoles--;
            continue;
        }
        //masahat hole ra barresi mikonad
        Area = (holes[i].P2.x - holes[i].P1.x+1)*(holes[i].P2.y - holes[i].P1.y+1);
        if (!( Min <= Area && Area <= Max ))
            numberOfHoles--;
    }
    delete[] holes;
    return (numberOfHoles>1);

}

bool inSpan(double x, double center, double radius)
{
    return (center-radius <= x) && (x <= center+radius);
}

vector <Component> GetValidComponents(bool **skinMatrix ,unsigned int width, unsigned int height, Component *ConnectedComponents, int numberOfComponents, int& numberOfFaces)
{
    //test haye mokhtalif ra baraye tashkhis

    vector <Component> ValidComponent;
    numberOfFaces = 1;
    unsigned int PercentOfSkin;

    const double GoldenNumber = 1.6;

    unsigned int h, w;
    bool **Matrix;

    // Moteghayer haye halghe
    int i;
    unsigned int m,k,l;
    //component ha ra barresi mikonad
    for (i = 0; i < numberOfComponents; i++)
    {

        h = ConnectedComponents[i].P2.y - ConnectedComponents[i].P1.y + 1; // h = dy + 1 (max - min + 1)
        w = ConnectedComponents[i].P2.x - ConnectedComponents[i].P1.x + 1; // w = dx + 1 (max - min + 1)

        // Azmoone masahat
        if (!( ProgramOptions.options.optComponentAreaMin <= w*h && w*h <= ProgramOptions.options.optComponentAreaMax ))
        {
            if (w*h > 60)
            _L("#%03d rejected - %s (%5d) (%3d,%3d) -> (%3d,%3d)", i, "Area test", w*h, ConnectedComponents[i].P1.x, ConnectedComponents[i].P1.y, ConnectedComponents[i].P2.x, ConnectedComponents[i].P2.y)
            continue;
        }

        // agar soorat ofoghi ham bashad kar mikonad
        if
        (!inSpan(1.f*h/w, GoldenNumber, ProgramOptions.options.optGoldenNumberError )
           &&
         !( 0.8 <= 1.f*h/w && 1.f*h/w <= GoldenNumber))

        {
            _L("\n#%3d#(%3d,%3d) -> (%3d,%3d)", i, ConnectedComponents[i].P1.x, ConnectedComponents[i].P1.y, ConnectedComponents[i].P2.x, ConnectedComponents[i].P2.y)
            _L("#%03d rejected - %s (%1.4f)", i, "Golden Ratio test", 1.f*h/w)
            continue;
        }

        //gesmati az skinMatrix ra ke marboot be in component ast dar Matrix mirizad
        _L("GetValidComponents -> memory allocation")
        Matrix = new bool*[h];
        for (m = 0; m < h; m++)
            Matrix[m]= new bool[w];

        for (k = 0; k < h ; k++)
        for (l = 0; l < w ; l++)
            Matrix[k][l] = skinMatrix[k+ConnectedComponents[i].P1.y][l+ConnectedComponents[i].P1.x];

        // Barresie Matrix

        //nesbate roshan ra be kol hesab mikonad
        PercentOfSkin = GetSkinPercent(Matrix, &ConnectedComponents[i]);
        // agar edge detection faal bashad, darsade poost betore khodkar kam mishavad,
        // pas sathe tavaghoe barname bayad paiin byayad
        if (ProgramOptions.options.optEdgeDetection)
            ProgramOptions.options.optSkinPercentMin -= 8;
        if (PercentOfSkin > ProgramOptions.options.optSkinPercentMax || PercentOfSkin < ProgramOptions.options.optSkinPercentMin)
        {
            _L("\n#%3d#(%3d,%3d) -> (%3d,%3d)", i, ConnectedComponents[i].P1.x, ConnectedComponents[i].P1.y, ConnectedComponents[i].P2.x, ConnectedComponents[i].P2.y)
            _L("#%03d rejected - %s(%02u)", i, "Skin Percent test", PercentOfSkin)
            goto FreeMatrix;
        }

        //soorakh ha ra barresi mikonad
        if (!Checkholes(Matrix, w, h))
        {
            _L("\n#%3d#(%3d,%3d) -> (%3d,%3d)", i, ConnectedComponents[i].P1.x, ConnectedComponents[i].P1.y, ConnectedComponents[i].P2.x, ConnectedComponents[i].P2.y)
            _L("#%03d rejected - %s", i, "Holes test")
            goto FreeMatrix;
        }

        //agar tamame sharayex bala bargharar bood component ra tayeed mikonad
        //va morattab mikonad
        ValidComponent.resize(ValidComponent.size());
        ValidComponent.push_back(ConnectedComponents[i]);
        _L("#%03d Accepted", i)
        numberOfFaces++;

        FreeMatrix:
        // free memory of matrix
        for (m = 0; m < h; m++)
            delete[] Matrix[m];
        delete[] Matrix;
    }

    numberOfFaces--;
    delete[] ConnectedComponents;
    return ValidComponent;
}
