/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: main.cpp                                     //
// @desc: contains main function                       //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/

#include "../include/ColorProcessor.h"
#include "../include/utils.h"
#include "../include/BMPImage.h"
#include "../include/ConnectedComponent.h"
#include "../include/ValidComponents.h"
#include "../include/EdgeDetector.h"

const char configFileAddress[] = "BMPImage.conf";
clsSettings ProgramOptions(configFileAddress);

void MakeMaskPicture(bool**, BMPImage*);

int main(int argc, char** argv)
{
    char *ExecutableName = NULL, \
         *fileAddress = NULL;

    bool retry = false;

    // Figure out file executable name
    ExecutableName = strdup(GetExecutableName(argv[0]));

    // Get file address
    if (argc == 1)
    {
        string input;
        string strFileAddress;
        do
        {
            retry = false;
            cout << "[1] Enter address of a file to detect faces" << endl;
            cout << "[2] Change options" << endl;
            cout << "[3] Show command line options help (then exit)" << endl;
            cout << "[4] Exit Program" << endl;
            #ifdef LINUX
            cout << "\033[1m";
            #endif
            cout << "What do you want to do: ";
            #ifdef LINUX
            cout << "\033[0m";
            #endif
            cin >> input;
            transform(input.begin(), input.end(), input.begin(), ::tolower);
                if (input == "1")
            {
                _P("Enter your bitmap file address: ");
                cin >> strFileAddress;
                fileAddress = strdup(strFileAddress.c_str());
            }
            else if (input == "2" || input == "o" || input == "option" || input == "options" || input == "setting" || input == "settings")
            {
                ShowOptionsMenu(true);
                retry = true;
            }
            else if (input == "3" || input == "h" || input == "help" || input == "f1")
            {
                ShowHelp(ExecutableName);
                free(ExecutableName);

                return EXIT_SUCCESS;
            }
            else if (input == "4" || input == "0" || input == "exit" || input == "quit" || input == "e" || input == "q")
            {
                free(fileAddress);
                free(ExecutableName);

                return EXIT_SUCCESS;
            }
            else
            {
                _E("Dude, `%s' is not a valid option!", input.c_str())
                retry = true;
            }
        }
        while (retry);
    }
    else if (argc == 2)
    {
        if (strcmp(argv[1], "-o") == 0 || \
           strcmp(argv[1], "-O") == 0 || \
           strcmp(argv[1], "--options") == 0)
        {
            ShowOptionsMenu(false);
            free(fileAddress);
            free(ExecutableName);
            return EXIT_SUCCESS;
        }

        if (strcmp(argv[1], "-h") == 0 || \
           strcmp(argv[1], "-H") == 0 || \
           strcmp(argv[1], "--help") == 0)
        {
            ShowHelp(ExecutableName);
            free(ExecutableName);
            return EXIT_SUCCESS;
        }
        else
            fileAddress = strdup(argv[1]);    // Get file address from command line args
    }
    else
    {
        _E("Wrong number of parameters")      // Wrong number of parameters
        ShowHelp(ExecutableName);
        free(ExecutableName);
        return EXIT_PARAMETERS_WRONG;
    }

    // Load Picture
    BMPImage image;
    _L("Loading Picture")
    image.LoadPicture(fileAddress);

    switch(image.LoadState)
    {
    case FileNotExist:
        _E("`%s' does not exist.", fileAddress)

        free(fileAddress);
        free(ExecutableName);
        return EXIT_FILE_NOT_EXIST;
    case FileNotReadable:
        _E("`%s' is not readable by this program.", fileAddress)
        free(fileAddress);
        free(ExecutableName);
        return EXIT_FILE_NOT_READABLE;
    case FileTypeNotValid:
        _E("`%s' is not a valid BMP file.", fileAddress)
        free(fileAddress);
        free(ExecutableName);
        return EXIT_FILE_TYPE_NOT_VALID;
    case LoadSuccess:
        _L("Picture Loaded Successfully.")
        break;
    }

    if (ProgramOptions.options.optDebugMode)
        image.PrintFileInfo();
    //
    _L("Color Processing started")
    ColorProcessor imageColors = ColorProcessor(&image);
    imageColors.FilterSkinColor();

    _L("Skin-colored pixels are determined")
    //
    if (ProgramOptions.options.optEdgeDetection)
    {
        _L("Blur picture and Detect edges")
        DetectEdges(blur(image.pixelsMatrix, image.info.biWidth, image.info.biHeight, ProgramOptions.options.optBlurRadius), imageColors.skin, image.info.biWidth, image.info.biHeight);
    }
    //
    if(ProgramOptions.options.optMaskPicture)
        MakeMaskPicture(imageColors.skin, &image);
    //
    Component* connectedPixels;
    int numberOfConnectedPixels = 0;

    connectedPixels = GetConnectedComponents(imageColors.skin, image.info.biWidth, image.info.biHeight, numberOfConnectedPixels);
    _L("%02d Connected Components found", numberOfConnectedPixels)

    vector<Component> actualFaces;
    int numberOfFaces = 0;
    actualFaces     = GetValidComponents   (imageColors.skin, image.info.biWidth, image.info.biHeight, connectedPixels, numberOfConnectedPixels, numberOfFaces);
    _L("%02d Faces was among them", numberOfFaces)
    //
    BITMAPFILEHEADER outputHeader;
    BITMAPINFOHEADER outputInfo;
    //
    string outputFileAddress(fileAddress);
    int lastDot = outputFileAddress.find_last_of(".");
    outputFileAddress = outputFileAddress.substr(0, lastDot);
    outputFileAddress += string(".detected");
    char buf[5];
    sprintf(buf,"%d",ProgramOptions.options.optBlurRadius);
    if (ProgramOptions.options.optEdgeDetection)
        outputFileAddress += string("E") + string(buf);
    outputFileAddress += string(".bmp");
    //
    if (numberOfFaces == 0)
    {
        _P("Oh! No faces found in this picture\n")
        free(fileAddress);
        free(ExecutableName);
        return EXIT_SUCCESS;
    }
    else
    {
        _P("There is %02d faces in the picture.\ncheck this files: %s\n", numberOfFaces, outputFileAddress.c_str());
    }

    BMPImage outputImage;

    //make the output file
    outputHeader = image.header;
    outputInfo = image.info;

    outputHeader.bfReserved1 = 0x4242; // MM
    outputHeader.bfReserved2 = 0x4253; // MS
    outputHeader.bfOffBits = HEADER_SIZE + INFO_SIZE;

    if (!outputImage.CreatePicture(outputFileAddress.c_str(), &outputHeader, &outputInfo))
    {
        _E("Cannot create or write into output file(%s). Program will now exit", outputFileAddress.c_str())
        return EXIT_CANNOT_CREATE_OUTPUT;
    }

    _L("Copying source picture")
    outputImage.CopyPicture(&image);

    _L("Drawing rectangle around the faces")
    RGBPIXEL blue;
    blue.rgbRed = 44;
    blue.rgbGreen = 133;
    blue.rgbBlue = 255;
    outputImage.ShowComponents(&actualFaces, &blue);
    _L("Writing data from memory to output file")

    outputImage.WriteImageData();
    //
    free(fileAddress);
    free(ExecutableName);
    //
    return EXIT_SUCCESS;
}


void MakeMaskPicture(bool** maskBool, BMPImage* sourceImage)
{
    BMPImage outputImage;

    string outputFileAddress(sourceImage->fileAddress);
    int lastDot = outputFileAddress.find_last_of(".");
    outputFileAddress = outputFileAddress.substr(0, lastDot);
    outputFileAddress += string(".mask");
    outputFileAddress += string(ProgramOptions.options.optEdgeDetection ? "EdgeDetection" : "");
    outputFileAddress += string(".bmp");

    outputImage.CreatePicture(outputFileAddress.c_str(), &sourceImage->header, &sourceImage->info);

    int row, rows = sourceImage->info.biHeight;
    int col, cols = sourceImage->info.biWidth;

    _L("Making mask file")
    for (row = 0 ; row < rows ; row++)
    for (col = 0 ; col < cols ; col++)
        if (maskBool[row][col])
        {
            outputImage.pixelsMatrix[row][col].rgbRed = 0;
            outputImage.pixelsMatrix[row][col].rgbGreen = 255;
            outputImage.pixelsMatrix[row][col].rgbBlue = 45;
        }
        else
        {
            outputImage.pixelsMatrix[row][col].rgbRed = 0;
            outputImage.pixelsMatrix[row][col].rgbGreen = 0;
            outputImage.pixelsMatrix[row][col].rgbBlue = 0;
        }
    _L("Writing into mask file")
    outputImage.WriteImageData();
}
