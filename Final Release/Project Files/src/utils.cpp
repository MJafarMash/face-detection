/*******************************************************/
//                                                     //
//               Face Detection Program                //
// Fall 2013 / 1391 - Sharif University of Technology  //
// C++ programming project - course ID: 40153          //
//                                                     //
// @file: utils.cpp                                    //
// @desc: some misc functions                          //
// @author: MohammadJafar MashhadiEbrahim              //
//                                                     //
/*******************************************************/

#include "../include/utils.h"

extern clsSettings ProgramOptions;

bool FileExists(const char* fileAddress)
{
#ifdef LINUX
    return (access( fileAddress , F_OK ) == 0);
#endif
#ifdef MSWINDOWS
    return (_access( fileAddress , F_OK ) == 0);
#endif
}

bool FileReadable(const char* fileAddress)
{
#ifdef LINUX
    return (access( fileAddress , R_OK ) == 0);
#endif
#ifdef MSWINDOWS
    return (_access( fileAddress , R_OK ) == 0);
#endif
}

void MixWithColor(RGBPIXEL* destPixel, RGBPIXEL* mixPixel)
{
    destPixel->rgbRed = (mixPixel->rgbRed + destPixel->rgbRed) / 2;
    destPixel->rgbGreen = (mixPixel->rgbGreen + destPixel->rgbGreen) / 2;
    destPixel->rgbBlue = (mixPixel->rgbBlue + destPixel->rgbBlue) / 2;
}

void ShowHelp(const char* ExecutableName)
{
#ifdef LINUX
    _P("\033[1mNAME\033[0m\n")
    _P("\t%s - Detects faces in a bitmap file and saves the output in another file\n", ExecutableName);
    _P("\033[1mSYNOPSIS\033[0m\n")
    _P("\t\033[1m%s\033[0m [/path/to/file]\n", ExecutableName)
    _P("\t\033[1m%s\033[0m [OPTIONS]\n", ExecutableName)
    _P("\033[1mOPTIONS\033[0m\n")
    _P("\t\033[1m-o\033[0m, \033[1m-O\033[0m\n\t\033[1m--options\033[0m\n\t\tShow and edit program options\n")
    _P("\t\033[1m-h\033[0m, \033[1m-H\033[0m\n\t\033[1m--help\033[0m\n\t\tdisplay this help and exit\n")
#endif

#ifdef MSWINDOWS
    _P("NAME\n")
    _P("\t%s - Detects faces in a bitmap file and saves the output in another file\n", ExecutableName);
    _P("SYNOPSIS\n")
    _P("\t%s [X:\\path\\to\\file]", ExecutableName)
    _P("\t%s [OPTIONS]\n", ExecutableName)
    _P("OPTIONS\n")
    _P("\t-o, -O\n\t--options\n\t\tShow and edit program options\n")
    _P("\t-h, -H\n\t--help\n\t\tdisplay this help and exit\n")
#endif
}

char* GetExecutableName(char* argv0)
{
    char* buffer;

    buffer = strrchr (argv0, PATH_SEPARATOR);
    if (!buffer)
        buffer = strdup(argv0);
    else
        ++buffer;

    return buffer;
}
